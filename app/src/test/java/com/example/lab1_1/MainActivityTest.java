package com.example.lab1_1;

import junit.framework.TestCase;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MainActivityTest extends TestCase {
    @Test
    public void testMinFunc1() {
        FuncManager temp = new FuncManager();
        int min = temp.minFunc(1,2);
        assertEquals(1, min);
    }
    @Test
    public void testMaxFunc1() {
        FuncManager temp = new FuncManager();
        int max = temp.maxFunc(1,2);
        assertEquals(2, max);
    }
    @Test
    public void testMinFunc2() {
        FuncManager temp = new FuncManager();
        int min = temp.minFunc(1,1);
        assertEquals(1, min);
    }
    @Test
    public void testMaxFunc2() {
        FuncManager temp = new FuncManager();
        int max = temp.maxFunc(2,2);
        assertEquals(2, max);
    }
    @Test
    public void testMinFunc3() {
        FuncManager temp = new FuncManager();
        int min = temp.minFunc(1,2);
        assertEquals(1, min);
    }
    @Test
    public void testMaxFunc3() {
        FuncManager temp = new FuncManager();
        int max = temp.maxFunc(1,2);
        assertEquals(2, max);
    }
    @Test
    public void testMinFunc4() {
        FuncManager temp = new FuncManager();
        int min = temp.minFunc(1,2);
        assertEquals(1, min);
    }
    @Test
    public void testMaxFunc4() {
        FuncManager temp = new FuncManager();
        int max = temp.maxFunc(1,2);
        assertEquals(2, max);
    }
}