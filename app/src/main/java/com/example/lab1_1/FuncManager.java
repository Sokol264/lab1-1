package com.example.lab1_1;

public class FuncManager {
    int minFunc(int a, int b) {
        return (a < b) ? a : b;
    }

    int maxFunc(int a, int b) {
        return (a > b) ? a : b;
    }
}
